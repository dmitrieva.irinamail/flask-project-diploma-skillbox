# coding: utf-8
import os
from datetime import datetime
from typing import List, Optional
from uuid import uuid1

from flask import request
from flask_sqlalchemy.model import DefaultMeta
from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Integer,
    Sequence,
    String,
    Table,
    or_,
)
from sqlalchemy.exc import NoResultFound  # type: ignore
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

from .database import db
from .errors import (
    AccessDeniedError,
    MediaExistenceError,
    TweetExistenceError,
    UserError,
)
from .settings import ALLOWED_EXTENSIONS, get_media_directory

BaseModel: DefaultMeta = db.Model

follows_table = Table(
    "follows",
    db.metadata,
    Column("who_following_id", ForeignKey("users.id"), primary_key=True),
    Column("followed_id", ForeignKey("users.id"), primary_key=True),
)

likes_table = Table(
    "likes",
    db.metadata,
    Column("user_id", ForeignKey("users.id"), primary_key=True),
    Column("tweet_id", ForeignKey("tweets.id"), primary_key=True),
)


class User(BaseModel):  # type: ignore
    """Model of users table."""

    __tablename__ = "users"

    id = Column(Integer, Sequence("users_id_idx"), primary_key=True)
    name = Column(String(140), nullable=False, unique=True)
    api_key = Column(String(140), nullable=False, unique=True)

    user_tweets = relationship(
        "Tweet",
        back_populates="user",
        cascade="all, delete-orphan",
        lazy="joined",
    )
    user_following = relationship(
        "User",
        secondary=follows_table,
        primaryjoin=(id == follows_table.c.who_following_id),
        secondaryjoin=(id == follows_table.c.followed_id),
        lazy="joined",
    )
    user_followed_by = relationship(
        "User",
        secondary=follows_table,
        primaryjoin=(id == follows_table.c.followed_id),
        secondaryjoin=(id == follows_table.c.who_following_id),
        lazy="joined",
    )
    tweet_likes = relationship(
        "Tweet",
        secondary=likes_table,
        back_populates="users_likes",
        lazy="joined",
    )


class Media(BaseModel):  # type: ignore
    """Model of medias table."""

    __tablename__ = "medias"

    id = Column(Integer, Sequence("media_id_idx"), primary_key=True)
    tweet_id: Column[Optional[int]] = Column(ForeignKey("tweets.id"))
    file_path = Column(String, nullable=False)
    file_name = Column(String, nullable=False)

    tweet = relationship(
        "Tweet",
        back_populates="tweet_medias",
        lazy="joined",
    )


class Tweet(BaseModel):  # type: ignore
    """Model of tweets table."""

    __tablename__ = "tweets"

    id = Column(Integer, Sequence("tweet_id_idx"), primary_key=True)
    data = Column(String(140), nullable=False)
    date = Column(DateTime, nullable=False)
    user_id: Column[int] = Column(ForeignKey("users.id"), nullable=False)

    user = relationship(
        "User",
        back_populates="user_tweets",
        lazy="joined",
    )
    tweet_medias = relationship(
        "Media",
        back_populates="tweet",
        cascade="all, delete-orphan",
        lazy="joined",
        order_by=lambda: Media.id,
    )
    users_likes = relationship(
        "User",
        secondary=likes_table,
        back_populates="tweet_likes",
        lazy="joined",
    )

    @hybrid_property
    def tweet_media_links(self) -> list:
        """Return links to media files for this tweet."""
        return ["/api/medias/" + str(media.id) for media in list(self.tweet_medias)]


def allowed_file(filename) -> bool:
    """Indicate whether a filename can be used in a media file."""
    return "." in filename and filename.rsplit(".", 1)[1] in ALLOWED_EXTENSIONS


def check_user(func):
    """Check user api-key from request."""

    def wrapper(*args, **kwargs):
        api_key = request.headers.get("api-key")
        user = db.session.query(User).filter(User.api_key == api_key).first()
        if not user:
            raise UserError("For this api-key there are no users in db!")

        try:
            res = func(*args, user=user, **kwargs)
        except TypeError as exc:
            if "got multiple values for argument 'user'" in str(exc):
                import inspect

                args_names = inspect.getfullargspec(func).args
                kwargs.update(dict(zip(args_names, args)))
                kwargs["user"] = user
                res = func(**kwargs)
            elif "got an unexpected keyword argument 'user'" in str(exc):
                res = func(*args, **kwargs)
            else:
                raise exc
        return res

    return wrapper


@check_user
def add_media(file: FileStorage) -> Media:
    """Save file directory and add path to it in db."""
    media_directory = get_media_directory()
    file_filename: str = file.filename if file.filename else ""
    filename, file_extension = os.path.splitext(secure_filename(file_filename))
    new_filename = f"{filename}_{uuid1()}{file_extension}"
    media_path = os.path.join(media_directory, new_filename)
    media_path = media_path.replace(" ", "_")
    media_path = media_path.lower()
    if not os.path.exists(media_directory):
        os.makedirs(media_directory)
    file.save(dst=media_path)
    new_rec = Media(file_path=media_directory, file_name=new_filename)
    db.session.add(new_rec)
    db.session.commit()
    return new_rec


@check_user
def add_tweet(record: dict, user: User) -> Tweet:
    """Create tweet record."""
    new_rec = Tweet(data=record["tweet_data"], date=datetime.now(), user_id=user.id)
    db.session.add(new_rec)

    if record.get("tweet_media_ids"):
        db.session.flush()
        medias = (
            db.session.query(Media)
            .filter(Media.id.in_(record["tweet_media_ids"]))
            .all()
        )
        for media in medias:
            media.tweet_id = new_rec.id
    db.session.commit()
    return new_rec


def delete_tweet(tweet_id: int) -> None:
    """Delete tweet record."""
    api_key = request.headers.get("api-key")
    tweet = db.session.query(Tweet).filter(Tweet.id == tweet_id).first()
    if not tweet:
        raise TweetExistenceError(f"Tweet with {tweet_id=} does not exists!")
    if tweet.user.api_key != api_key:
        raise AccessDeniedError("You can delete only personal tweets!")

    for media in tweet.tweet_medias:
        os.remove(os.path.join(media.file_path, media.file_name))
    db.session.delete(tweet)
    db.session.commit()


@check_user
def get_tweets(user: User) -> List[Tweet]:
    """Get tweet records sorted by likes."""
    who_i_follow = (
        db.session.query(follows_table.c.followed_id)
        .join(User, User.id == follows_table.c.who_following_id)
        .filter(User.api_key == user.api_key)
        .subquery()
    )

    tweets: List[Tweet] = (
        db.session.query(Tweet)
        .join(Tweet.user)
        .filter(or_(User.api_key == user.api_key, User.id.in_(who_i_follow)))
        .all()
    )
    tweets.sort(key=lambda k: len(k.users_likes), reverse=True)
    return tweets


def get_media_store_info(media_id: int) -> tuple:
    """Obtain information about the storage of media data - file_path, file_name."""
    path_name = (
        db.session.query(Media.file_path, Media.file_name)
        .filter(Media.id == media_id)
        .first()
    )
    if path_name:
        file_path, file_name = path_name
        return file_path, file_name
    raise MediaExistenceError(f"Media with {media_id=} does not exists!")


@check_user
def get_user(user: User, user_id: int = None) -> User:
    """Get user's record by user_id in path or personal record."""
    if user_id:
        try:
            user = db.session.query(User).filter(User.id == user_id).one()
        except NoResultFound:
            raise UserError(f"No user with this data {user_id=}")
    return user


def follow_user(user_id: int) -> None:
    """Subscribe an authorized user to a user with a given user_id."""
    user_me = get_user()
    user_want_to_follow = get_user(user_id=user_id)
    if user_me.id == user_want_to_follow.id:
        raise UserError("You cant follow yourself!")
    user_me.user_following.append(user_want_to_follow)
    db.session.commit()


def unfollow_user(user_id: int = None) -> None:
    """Unsubscribe an authorized user from a user with a given user_id."""
    user_me = get_user()
    for following in user_me.user_following:
        if following.id == user_id:
            user_me.user_following.remove(following)
    db.session.commit()


def like_tweet(tweet_id: int = None) -> None:
    """Set an authorized user like on a tweet."""
    user_me = get_user()
    tweet = db.session.query(Tweet).get(tweet_id)
    if not tweet:
        raise TweetExistenceError(f"Tweet with {tweet_id=} does not exists!")
    tweet.users_likes.append(user_me)
    db.session.commit()


def dislike_tweet(tweet_id: int = None) -> None:
    """Delete an authorized user like on a tweet."""
    user_me = get_user()
    for tweet in user_me.tweet_likes:
        if tweet.id == tweet_id:
            user_me.tweet_likes.remove(tweet)
    db.session.commit()
