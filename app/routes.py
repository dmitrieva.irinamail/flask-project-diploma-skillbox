from typing import Dict, Tuple, Union

from flask import Response, make_response, render_template, request, send_from_directory
from flask_restful import Api, Resource
from marshmallow import ValidationError

from .errors import (
    AccessDeniedError,
    MediaExistenceError,
    TweetExistenceError,
    UserError,
)
from .models import (
    add_media,
    add_tweet,
    allowed_file,
    delete_tweet,
    dislike_tweet,
    follow_user,
    get_media_store_info,
    get_tweets,
    get_user,
    like_tweet,
    unfollow_user,
)
from .schemas import TweetSchema
from .settings import CSS_DIRECTORY, JS_DIRECTORY, TEMPLATE_FOLDER


class TweetList(Resource):
    """Endpoints for the route tweets."""

    def post(self) -> Tuple[dict, int]:
        """
        Создание нового твита.
        ---
        tags:
          - tweets
        parameters:
         - in: body
           name: new tweet params
           schema:
             $ref: '#/definitions/Tweet'
         - in: header
           name: api-key
           schema:
             type: string
             format: uuid
             default: test
           required: true
        responses:
          201:
            description: Tweets data
            schema:
              type: object
              properties:
                result:
                  type: boolean
                  default: true
                tweet_id:
                    type: integer
          400:
           description: Произошла ошибка
           schema:
             type: object
             properties:
               result:
                  type: boolean
                  default: false
               error_type:
                   type: string
               error_message:
                   type: string
        """

        data = request.json

        schema = TweetSchema()
        try:
            tweet = schema.load(data)
        except ValidationError as exc:
            return {
                "result": False,
                "error_type": "ValidationError",
                "error_message": exc.messages,
            }, 400
        try:
            tweet = add_tweet(tweet)
        except UserError as exc:
            return {
                "result": False,
                "error_type": type(exc).__name__,
                "error_message": str(exc),
            }, 400
        return {"result": True, "tweet_id": tweet.id}, 201

    def get(self) -> Tuple[dict, int]:
        """
        Получить ленту с твитами.
        ---
        tags:
          - tweets
        parameters:
         - in: header
           name: api-key
           schema:
             type: string
             format: uuid
             default: test
           required: true
        responses:
          201:
            description: Tweets data
            schema:
              type: object
              properties:
                result:
                  type: boolean
                  default: true
                tweets:
                  type: array
                  items:
                    type: object
                    properties:
                      id:
                        type : integer
                        format: int64
                        minimum: 1
                      content:
                        type : string
                        default: Tweet about weather
                      attachments:
                        type: array
                        items:
                          type : string
                      author:
                        type: object
                        properties:
                          id:
                            type : integer
                            format: int64
                            minimum: 1
                          name:
                            type : string
                      likes:
                        type: array
                        items:
                          type: object
                          properties:
                            user_id:
                              type : integer
                              format: int64
                              minimum: 1
                            name:
                              type : string

          400:
           description: Произошла ошибка
           schema:
             type: object
             properties:
               result:
                  type: boolean
                  default: false
               error_type:
                   type: string
               error_message:
                   type: string
        """
        try:
            tweets = get_tweets()
        except UserError as exc:
            return {
                "result": False,
                "error_type": type(exc).__name__,
                "error_message": str(exc),
            }, 400
        return {
            "result": True,
            "tweets": [
                {
                    "id": tweet.id,
                    "content": tweet.data,
                    "attachments": tweet.tweet_media_links,
                    "author": {"id": tweet.user_id, "name": tweet.user.name},
                    "likes": [
                        {"user_id": user.id, "name": user.name}
                        for user in tweet.users_likes
                    ],
                }
                for tweet in tweets
            ],
        }, 200


class Tweet(Resource):
    """Endpoints for the route tweet."""

    def delete(self, tweet_id: int) -> Tuple[dict, int]:
        """
        Удаление твита
        ---
        tags:
          - tweets
        parameters:
         - in: path
           name: tweet_id
           required: true
           description: Parameter ID for tweet.
           schema:
             type : integer
             format: int64
             minimum: 1
         - in: header
           name: api-key
           schema:
             type: string
             format: uuid
             default: test
           required: true
        responses:
          201:
            description: Delete result
            schema:
              type: object
              properties:
                result:
                  type: boolean
                  default: true
          400:
           description: Произошла ошибка
           schema:
             type: object
             properties:
               result:
                  type: boolean
                  default: false
               error_type:
                   type: string
               error_message:
                   type: string
        """
        try:
            delete_tweet(tweet_id)
        except (AccessDeniedError, TweetExistenceError) as exc:
            return {
                "result": False,
                "error_type": type(exc).__name__,
                "error_message": str(exc),
            }, 400
        return {"result": True}, 201


class Medias(Resource):
    """Endpoints for the route medias."""

    def post(self) -> Tuple[dict, int]:
        """
        Загрузка файлов из твита
        ---
        tags:
          - medias
        consumes:
         - multipart/form-data
        parameters:
          - in: formData
            name: file
            type: file
            description: The file to upload.
          - in: header
            name: api-key
            schema:
              type: string
              format: uuid
              default: test
            required: true
        responses:
          201:
            description: Tweets data
            schema:
              type: object
              properties:
                result:
                  type: boolean
                  default: true
                media_id:
                    type: integer
          400:
           description: Произошла ошибка
           schema:
             type: object
             properties:
               result:
                  type: boolean
                  default: false
               error_type:
                   type: string
               error_message:
                   type: string
        """
        files = request.files
        if not files or not files.get("file"):
            return {
                "result": False,
                "error_type": "ValidationError",
                "error_message": "File is empty",
            }, 400
        elif not allowed_file(files["file"].filename):
            return {
                "result": False,
                "error_type": "ValidationError",
                "error_message": "File is not allowed",
            }, 400
        media = add_media(files["file"])
        return {"result": True, "media_id": media.id}, 201


class Like(Resource):
    """Endpoints for the route like."""

    def post(self, tweet_id: int) -> Tuple[dict, int]:
        """
        Простановка лайка на твит
        ---
        tags:
          - likes
        parameters:
         - in: path
           name: tweet_id
           required: true
           description: Parameter ID for tweet.
           schema:
             type : integer
             format: int64
             minimum: 1
         - in: header
           name: api-key
           schema:
             type: string
             format: uuid
             default: test
           required: true
        responses:
          201:
            description: Like result
            schema:
              type: object
              properties:
                result:
                  type: boolean
                  default: true
          400:
           description: Произошла ошибка
           schema:
             type: object
             properties:
               result:
                  type: boolean
                  default: false
               error_type:
                   type: string
               error_message:
                   type: string
        """
        try:
            like_tweet(tweet_id)
        except (UserError, TweetExistenceError) as exc:
            return {
                "result": False,
                "error_type": type(exc).__name__,
                "error_message": str(exc),
            }, 400
        return {"result": True}, 201

    def delete(self, tweet_id: int) -> Tuple[dict, int]:
        """
        Убрать лайк с твита
        ---
        tags:
          - likes
        parameters:
         - in: path
           name: tweet_id
           required: true
           description: Parameter ID for tweet.
           schema:
             type : integer
             format: int64
             minimum: 1
         - in: header
           name: api-key
           schema:
             type: string
             format: uuid
             default: test
           required: true
        responses:
          201:
            description: Delete result
            schema:
              type: object
              properties:
                result:
                  type: boolean
                  default: true
          400:
           description: Произошла ошибка
           schema:
             type: object
             properties:
               result:
                  type: boolean
                  default: false
               error_type:
                   type: string
               error_message:
                   type: string
        """
        try:
            dislike_tweet(tweet_id)
        except UserError as exc:
            return {
                "result": False,
                "error_type": type(exc).__name__,
                "error_message": str(exc),
            }, 400
        return {"result": True}, 201


class Follow(Resource):
    """Endpoints for the route follow."""

    def post(self, user_id: int) -> Tuple[dict, int]:
        """
        Подписаться на пользователя
        ---
        tags:
          - follows
        parameters:
         - in: path
           name: user_id
           required: true
           description: Parameter ID for user.
           schema:
             type : integer
             format: int64
             minimum: 1
         - in: header
           name: api-key
           schema:
             type: string
             format: uuid
             default: test
           required: true
        responses:
          201:
            description: Follow result
            schema:
              type: object
              properties:
                result:
                  type: boolean
                  default: true
          400:
           description: Произошла ошибка
           schema:
             type: object
             properties:
               result:
                  type: boolean
                  default: false
               error_type:
                   type: string
               error_message:
                   type: string
        """
        try:
            follow_user(user_id)
        except UserError as exc:
            return {
                "result": False,
                "error_type": type(exc).__name__,
                "error_message": str(exc),
            }, 400
        return {"result": True}, 201

    def delete(self, user_id: int) -> Tuple[dict, int]:
        """
        Убрать подписку на пользователя
        ---
        tags:
          - follows
        parameters:
         - in: path
           name: user_id
           required: true
           description: Parameter ID for user.
           schema:
             type : integer
             format: int64
             minimum: 1
         - in: header
           name: api-key
           schema:
             type: string
             format: uuid
             default: test
           required: true
        responses:
          201:
            description: Delete result
            schema:
              type: object
              properties:
                result:
                  type: boolean
                  default: true
          400:
           description: Произошла ошибка
           schema:
             type: object
             properties:
               result:
                  type: boolean
                  default: false
               error_type:
                   type: string
               error_message:
                   type: string
        """
        try:
            unfollow_user(user_id)
        except UserError as exc:
            return {
                "result": False,
                "error_type": type(exc).__name__,
                "error_message": str(exc),
            }, 400
        return {"result": True}, 201


class User(Resource):
    """Endpoints for the route user."""

    def get(self, user_id: int = None) -> Tuple[dict, int]:
        """
        Получение информации о профиле
        ---
        tags:
          - users
        parameters:
         - in: header
           name: api-key
           schema:
             type: string
             format: uuid
             default: test
           required: true
         - in: path
           name: user_id
           description: Parameter ID for user, required if route contains user_idd
           schema:
             type : integer
             format: int64
             minimum: 1
        responses:
          201:
            description: Users data
            schema:
              type: object
              properties:
                result:
                  type: boolean
                  default: true
                user:
                  type: array
                  items:
                    type: object
                    properties:
                      id:
                        type : integer
                        format: int64
                        minimum: 1
                      name:
                        type : string
                      followers:
                        type: array
                        items:
                          type: object
                          properties:
                            id:
                              type : integer
                              format: int64
                              minimum: 1
                            name:
                              type : string
                      following:
                        type: array
                        items:
                          type: object
                          properties:
                            id:
                              type : integer
                              format: int64
                              minimum: 1
                            name:
                              type : string

          400:
           description: Произошла ошибка
           schema:
             type: object
             properties:
               result:
                  type: boolean
                  default: false
               error_type:
                   type: string
               error_message:
                   type: string
        """
        try:
            user = get_user(user_id=user_id)
        except UserError as exc:
            return {
                "result": False,
                "error_type": type(exc).__name__,
                "error_message": str(exc),
            }, 400
        return {
            "result": True,
            "user": {
                "id": user.id,
                "name": user.name,
                "followers": [
                    {"id": follower.id, "name": follower.name}
                    for follower in user.user_followed_by
                ],
                "following": [
                    {"id": following.id, "name": following.name}
                    for following in user.user_following
                ],
            },
        }, 200


class Index(Resource):
    """Endpoints for getting static index.html."""

    def get(self) -> Response:
        """Get static index.html."""

        return make_response(render_template("index.html"))


class JS(Resource):
    """Endpoints for getting static *.js."""

    def get(self, path) -> Response:
        """Get static *.js."""

        return send_from_directory(JS_DIRECTORY, path)


class CSS(Resource):
    """Endpoints for getting static *.css."""

    def get(self, path) -> Response:
        """Get static *.css."""

        return send_from_directory(CSS_DIRECTORY, path)


class Icon(Resource):
    """Endpoints for getting static favicon.ico."""

    def get(self) -> Response:
        """Get static favicon.ico"""
        return send_from_directory(TEMPLATE_FOLDER, "favicon.ico")


class Media(Resource):
    """Endpoints for the route media."""

    def get(self, media_id: int) -> Union[Response, Tuple[Dict[str, object], int]]:
        """
        Отображение файлов из твита
        ---
        tags:
          - medias
        parameters:
         - in: path
           name: media_id
           description: Parameter ID for media
           required: true
           schema:
             type : integer
             format: int64
             minimum: 1
        produces:
        - image/png
        - image/gif
        - image/jpg
        - image/jpeg
        responses:
          200:
            description: Image of tweet
          400:
           description: Произошла ошибка
           schema:
             type: object
             properties:
               result:
                  type: boolean
                  default: false
               error_type:
                   type: string
               error_message:
                   type: string
        """
        try:
            media_directory, media_path = get_media_store_info(media_id)
        except MediaExistenceError as exc:
            return {
                "result": False,
                "error_type": type(exc).__name__,
                "error_message": str(exc),
            }, 400
        return send_from_directory(media_directory, media_path)


def init_routes(api: Api) -> None:
    """Add endpoints to api."""
    api.add_resource(Index, "/")
    api.add_resource(JS, "/js/<path:path>")
    api.add_resource(CSS, "/css/<path:path>")
    api.add_resource(Icon, "/favicon.ico")
    api.add_resource(TweetList, "/api/tweets")
    api.add_resource(Tweet, "/api/tweets/<int:tweet_id>")
    api.add_resource(Medias, "/api/medias")
    api.add_resource(Media, "/api/medias/<int:media_id>")
    api.add_resource(Like, "/api/tweets/<int:tweet_id>/likes")
    api.add_resource(Follow, "/api/users/<int:user_id>/follow")
    api.add_resource(User, "/api/users/me", "/api/users/<int:user_id>")
