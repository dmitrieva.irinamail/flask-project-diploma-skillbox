from flasgger import Schema, fields
from marshmallow.validate import Length


class TweetSchema(Schema):
    """Scheme for creating a tweet."""

    id = fields.Int(dump_only=True)
    tweet_data = fields.Str(required=True, validator=Length(max=140))
    tweet_media_ids = fields.List(fields.Int(), required=False)
