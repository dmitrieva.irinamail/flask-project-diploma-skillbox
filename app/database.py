import os

from flask_sqlalchemy import SQLAlchemy

user = os.environ.get("POSTGRES_USER")
pwd = os.environ.get("POSTGRES_PASSWORD")
host = "db"
port = "5432"

db_uri = f"postgresql+psycopg2://{user}:{pwd}@{host}:{port}/twitter_clone"

db = SQLAlchemy()
