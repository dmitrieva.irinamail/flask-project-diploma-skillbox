from datetime import datetime

from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from flasgger import APISpec, Swagger
from flask import Flask
from flask_restful import Api
from sqlalchemy.exc import InternalError, ProgrammingError

from .database import db, db_uri
from .models import Tweet, User
from .routes import init_routes
from .schemas import TweetSchema
from .settings import TEMPLATE_FOLDER


def create_app() -> Flask:
    """Create flask api with specification, init database and routes."""
    app = Flask(__name__, template_folder=TEMPLATE_FOLDER)
    api = Api(app)
    spec = APISpec(
        title="TwitterClone",
        version="1.0.0",
        openapi_version="2.0",
        plugins=[
            FlaskPlugin(),
            MarshmallowPlugin(),
        ],
    )
    app.config["SQLALCHEMY_DATABASE_URI"] = db_uri
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.init_app(app)

    @app.before_first_request
    def before_request_func():
        try:
            res = db.session.query(User).all()
        except (ProgrammingError, InternalError):
            res = "No db"
        else:
            if res:
                return
        if res == "No db":
            db.create_all()
        users = [
            User(name="Vasya", api_key="user1"),
            User(name="Petya", api_key="user2"),
            User(name="Masha", api_key="user3"),
            User(name="Guest", api_key="test"),
        ]
        db.session.add_all(users)
        db.session.flush()
        users[0].user_following.extend([users[1], users[2]])
        first_tweet = Tweet(
            data="Hello in tweet-clone!",
            date=datetime.now(),
            user_id=users[3].id,
        )
        db.session.add(first_tweet)
        db.session.flush()
        first_tweet.users_likes.extend(users[:-1])
        db.session.commit()

    template = spec.to_flasgger(
        app,
        definitions=[TweetSchema],
    )

    Swagger(app, template=template)

    init_routes(api)

    return app
