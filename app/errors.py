class UserError(Exception):
    """Error of user's data."""

    pass


class AccessDeniedError(Exception):
    """No data access error due to different api-key."""

    pass


class TweetExistenceError(Exception):
    """Error of no tweet with given parameters."""

    pass


class MediaExistenceError(Exception):
    """Error of no media with given parameters."""

    pass
