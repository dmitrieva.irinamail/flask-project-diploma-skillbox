import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ALLOWED_EXTENSIONS = {"png", "jpg", "jpeg", "gif"}
TEMPLATE_FOLDER = os.path.join(ROOT_DIR, "templates")
INDEX = os.path.join(TEMPLATE_FOLDER, "index.html")
JS_DIRECTORY = os.path.join(TEMPLATE_FOLDER, "js")
CSS_DIRECTORY = os.path.join(TEMPLATE_FOLDER, "css")


def get_media_directory() -> str:
    """Возвращает путь для сохранения медиа файлов."""
    return os.environ.get("MEDIA_DIR", os.path.join(ROOT_DIR, "media"))
