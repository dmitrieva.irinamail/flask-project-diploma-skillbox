# Сервис микроблогов twitter-clone



## Описание

В данном учебном проекте реализован бэкенд сервиса микроблогов, имеющего сходства с Twitter.

Сервис написан на языке <strong>python3.9.14</strong> с одновным стэком: <strong>flask, postgresql, sqlalchemy, marshmallow</strong>.

Тестирование реализовано с помощью <strong>pytest, factory-boy</strong>.

Код проверен линтерами <strong>mypy, flake8 (wemake-python-styleguide)</strong> и отформатированы с помощью <strong>black</strong> и <strong>isort</strong>.

Описана документация openapi при помощи <strong>flasgger, apispec</strong>.

Сервис развернут через <strong>docker compose</strong>.

## Основные команды
### Запуск
Для запуска контейнеров использовать команду:
```
docker compose up -d
```
Далее посетить основную страницу:
- [ ] [Ссылка на главную страницу](http://localhost:5000/)

### Документация
При <strong>запущенном</strong> контейнере доступна документация:
- [ ] [Ссылка на документацию](http://localhost:5000/apidocs)

### Тестирование
Тестирование запускается автоматически в данном инструменте <strong>Gitlab CI</strong>.
Но тесты можно запустить локально при начилии запущенного postgresql сервиса на машине и установленных пакетов tests/requirements-dev.txt:

```
python -m pytest tests
```
### Проверка линтерами и форматтерами
Проверка линтерами и форматтерами запускается автоматически в данном инструменте <strong>Gitlab CI</strong>.
Но проверку можно запустить локально при начилии установленных пакетов tests/requirements-dev.txt:
```
mypy app/
```
```
black --diff --check app/
```
```
isort --check-only --profile black app/
```
```
flake8 app/
```
