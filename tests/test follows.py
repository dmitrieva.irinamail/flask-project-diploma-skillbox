from app.models import User


def test_follow_user(client, db) -> None:
    """Проверка подписки на пользователя"""
    user_1, user_2 = db.session.query(User).filter(User.id.in_([1, 2])).all()
    resp = client.post(
        f"/api/users/{user_1.id}/follow", headers={"api_key": user_2.api_key}
    )
    assert resp.status_code == 201
    assert resp.json == {"result": True}

    assert user_2.user_following == [user_1]


def test_follow_self(client, db) -> None:
    """Проверка подписки на себя"""
    user = db.session.query(User).get(1)
    resp = client.post(
        f"/api/users/{user.id}/follow", headers={"api_key": user.api_key}
    )
    assert resp.status_code == 400
    assert resp.json["result"] is False
    assert resp.json.get("error_type")
    assert resp.json.get("error_message")

    assert user.user_following == []


def test_unfollow_user(client, db) -> None:
    """Проверка удаления подписки на пользователя"""
    user_1, user_2 = db.session.query(User).filter(User.id.in_([1, 2])).all()
    user_2.user_following = [user_1]
    db.session.commit()
    resp = client.delete(
        f"/api/users/{user_1.id}/follow", headers={"api_key": user_2.api_key}
    )
    assert resp.status_code == 201
    assert resp.json == {"result": True}

    assert user_2.user_following == []
