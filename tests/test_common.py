import os

import pytest

from app.models import User


def add_following(db) -> tuple:
    user_1, user_2 = db.session.query(User).filter(User.id.in_([1, 2])).all()

    user_1.user_following.append(user_2)
    db.session.commit()
    return user_1, user_2


@pytest.mark.parametrize("route", ["/api/tweets", "/api/users/me", "/api/users/1"])
def test_route_status(client_api_key, route):
    rv = client_api_key.get(route)
    assert rv.status_code == 200


@pytest.mark.parametrize("route", ["/api/tweets", "/api/users/me"])
def test_route_bad_api_key(client, route):
    api_key = "9dusdyfsgf87tgfs78"
    resp = client.get(route, headers={"api_key": api_key})
    assert resp.status_code == 400
    assert resp.json["result"] is False
    assert resp.json.get("error_type")
    assert resp.json.get("error_message")


@pytest.mark.parametrize(
    "route",
    ["/api/tweets", "/api/medias", "/api/users/1/follow", "/api/tweets/1/likes"],
)
def test_route_bad_api_key_post(client, route):
    api_key = "9dusdyfsgf87tgfs78"
    json = {}
    if route == "/api/tweets":
        json["tweet_data"] = "test"

    resp = client.post(route, headers={"api_key": api_key}, json={})
    assert resp.status_code == 400
    assert resp.json["result"] is False
    assert resp.json.get("error_type")
    assert resp.json.get("error_message")


@pytest.mark.parametrize(
    "route", ["/api/tweets/1", "/api/users/1/follow", "/api/tweets/1/likes"]
)
def test_route_bad_api_key_delete(client, route):
    api_key = "9dusdyfsgf87tgfs78"
    resp = client.delete(route, headers={"api_key": api_key})
    assert resp.status_code == 400
    assert resp.json["result"] is False
    assert resp.json.get("error_type")
    assert resp.json.get("error_message")


def test_render_jinja2(client_api_key, captured_templates) -> None:
    route = "/"
    resp = client_api_key.get(route)
    assert resp.status_code == 200
    assert len(captured_templates) == 1
    template, context = captured_templates[0]
    assert template.name == "index.html"


root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
css_files = os.listdir(os.path.join(root_dir, "app", "templates", "css"))
js_files = os.listdir(os.path.join(root_dir, "app", "templates", "js"))
js_files = [name for name in js_files if name[-4:] != ".map"]


@pytest.mark.parametrize("template", [*css_files, *js_files])
def test_render_css_js(client_api_key, template) -> None:
    file_name, file_extension = os.path.splitext(template)
    route = f"/{file_extension[1:]}/{template}"
    resp = client_api_key.get(route)
    assert resp.status_code == 200
