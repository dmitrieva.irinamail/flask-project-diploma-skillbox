import os
import shutil
from wsgiref.headers import Headers

import pytest
from flask import template_rendered, testing
from werkzeug.datastructures import Headers

from app import create_app
from app.database import db as _db
from app.models import User
from tests.factories import TweetFactory

root_dir = os.path.dirname(os.path.abspath(__file__))
media_dir = os.path.join(root_dir, "media_test")


@pytest.fixture
def app():
    os.environ["MEDIA_DIR"] = media_dir
    os.makedirs(media_dir, exist_ok=True)

    _app = create_app()
    _app.config["TESTING"] = True

    user = os.environ.get("POSTGRES_USER")
    pwd = os.environ.get("POSTGRES_PASSWORD")
    database = os.environ.get("POSTGRES_DB")
    if user and pwd and database:
        # Gitlab CI
        host = "postgres"
        db_uri = f"postgresql+psycopg2://{user}:{pwd}@{host}/{database}"
    else:
        # Local tests
        db_uri = "postgresql+psycopg2://"
    _app.config["SQLALCHEMY_DATABASE_URI"] = db_uri

    with _app.app_context():
        _db.create_all()
        for _ in range(4):
            TweetFactory.create()

        yield _app
        _db.session.close()
        _db.drop_all()
        del os.environ["MEDIA_DIR"]
        shutil.rmtree(media_dir, ignore_errors=False)


class TestClient(testing.FlaskClient):
    def open(self, *args, **kwargs):
        api_key = _db.session.query(User.api_key).limit(1).scalar()
        api_key_headers = Headers([("api-key", api_key)])
        headers = kwargs.pop("headers", Headers())
        headers.extend(api_key_headers)
        kwargs["headers"] = headers
        return super().open(*args, **kwargs)


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


@pytest.fixture
def client_api_key(app):
    app.test_client_class = TestClient
    client = app.test_client()
    yield client


@pytest.fixture
def db(app):
    with app.app_context():
        yield _db


@pytest.fixture
def captured_templates(app):
    recorded = []

    def record(sender, template, context, **extra):
        recorded.append((template, context))

    template_rendered.connect(record, app)
    try:
        yield recorded
    finally:
        template_rendered.disconnect(record, app)
