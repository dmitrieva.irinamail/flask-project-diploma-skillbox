import datetime

import factory

from app.database import db
from app.models import Tweet, User


class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = User
        sqlalchemy_session = db.session

    name = factory.Faker("first_name", max_length=140, locale="ru_RU")
    api_key = factory.Faker("slug")


class TweetFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Tweet
        sqlalchemy_session = db.session

    data = factory.Faker("text", max_nb_chars=140, locale="ru_RU")
    date = factory.Faker(
        "date_time_ad",
        # tzinfo=datetime.tzinfo(),
        start_datetime=datetime.datetime(day=21, month=3, year=2006),
        end_datetime=datetime.datetime.now(),
    )
    user = factory.SubFactory(UserFactory)
