from app.models import Tweet, User


def test_like_tweet(client, db) -> None:
    """Проверка простановки лайка на твит"""
    user = db.session.query(User).get(1)
    tweet = db.session.query(Tweet).get(2)

    resp = client.post(
        f"/api/tweets/{tweet.id}/likes", headers={"api_key": user.api_key}
    )
    assert resp.status_code == 201
    assert resp.json == {"result": True}

    assert tweet.users_likes == [user]


def test_like_tweet_err(client, db) -> None:
    """Проверка простановки лайка на твит дважды"""
    user = db.session.query(User).get(1)
    tweet = db.session.query(Tweet).get(2)
    tweet.users_likes = [user]
    db.session.commit()

    resp = client.post(
        f"/api/tweets/{tweet.id}/likes", headers={"api_key": user.api_key}
    )
    assert resp.status_code == 201
    assert resp.json == {"result": True}

    assert tweet.users_likes == [user]


def test_dislike_tweet(client, db) -> None:
    """Проверка простановки лайка на твит дважды"""
    user = db.session.query(User).get(1)
    tweet = db.session.query(Tweet).get(2)
    tweet.users_likes = [user]
    db.session.commit()

    resp = client.delete(
        f"/api/tweets/{tweet.id}/likes", headers={"api_key": user.api_key}
    )
    assert resp.status_code == 201
    assert resp.json == {"result": True}

    assert tweet.users_likes == []
