from app.models import User
from tests.test_common import add_following


def test_user_me_json(client, db) -> None:
    """Проверка выдачи информации о текущем пользователе"""
    user_1, user_2 = add_following(db)

    # add follower
    user_3 = db.session.query(User).get(3)
    user_1.user_followed_by.append(user_3)
    db.session.commit()

    api_key = user_1.api_key
    resp = client.get("/api/users/me", headers={"api_key": api_key})
    assert resp.status_code == 200
    assert resp.json == {
        "result": True,
        "user": {
            "id": user_1.id,
            "name": user_1.name,
            "followers": [{"id": user_3.id, "name": user_3.name}],
            "following": [{"id": user_2.id, "name": user_2.name}],
        },
    }


def test_user_me_changes(client, db) -> None:
    """Проверка выдачи информации о текущем пользователе при смене пользователя"""
    user_1, user_2 = db.session.query(User).filter(User.id.in_([1, 2])).all()

    api_key_1 = user_1.api_key
    api_key_2 = user_2.api_key
    resp_1 = client.get("/api/users/me", headers={"api_key": api_key_1})
    resp_2 = client.get("/api/users/me", headers={"api_key": api_key_2})
    assert resp_1.status_code == 200
    assert resp_2.status_code == 200
    assert resp_1.json != resp_2.json


def test_user_info_json(client_api_key, db) -> None:
    """Проверка выдачи информации о пользователе"""
    user = db.session.query(User).first()

    resp = client_api_key.get(f"/api/users/{user.id}")
    assert resp.status_code == 200
    assert resp.json == {
        "result": True,
        "user": {"id": user.id, "name": user.name, "followers": [], "following": []},
    }


def test_user_unidentified_err(client_api_key, db) -> None:
    """Проверка выдачи информации о пользователе, которого нет в БД"""
    resp = client_api_key.get(f"/api/users/9999")
    assert resp.status_code == 400
    assert resp.json["result"] is False
    assert resp.json.get("error_type")
    assert resp.json.get("error_message")
