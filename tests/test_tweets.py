import os.path

from app.models import Media, Tweet, User
from tests.conftest import media_dir
from tests.test_common import add_following


def test_tweets_json(client, db) -> None:
    """Проверка выдачи твитов"""
    tweet = db.session.query(Tweet).get(1)
    user = db.session.query(User).get(2)
    api_key = tweet.user.api_key
    tweet.tweet_medias = [
        Media(id=1, file_path="/", tweet_id=tweet.id, file_name="img1.png"),
        Media(id=2, file_path="/", tweet_id=tweet.id, file_name="img2.png"),
    ]
    tweet.users_likes = [user]
    db.session.commit()
    resp = client.get("/api/tweets", headers={"api_key": api_key})
    assert resp.status_code == 200
    assert resp.json["result"]
    assert len(resp.json["tweets"]) == 1
    assert resp.json["tweets"][0]["id"] == tweet.id
    assert isinstance(resp.json["tweets"][0]["content"], str)
    assert resp.json["tweets"][0]["attachments"] == tweet.tweet_media_links
    assert resp.json["tweets"][0]["author"] == {
        "id": tweet.user.id,
        "name": tweet.user.name,
    }
    assert resp.json["tweets"][0]["likes"] == [{"user_id": user.id, "name": user.name}]


def test_tweets_see_followers(client, db) -> None:
    """Проверка отображения твитов пользователей, на которых подписан пользователь"""
    user_1, user_2 = add_following(db)

    api_key = user_1.api_key
    resp = client.get("/api/tweets", headers={"api_key": api_key})
    assert resp.status_code == 200
    resp_tweets = {tweet["id"] for tweet in resp.json["tweets"]}
    users_tweets = {tweets.id for tweets in user_1.user_tweets + user_2.user_tweets}
    assert resp_tweets == users_tweets


def test_create_tweet_without_media(client_api_key, db) -> None:
    """Проверка создания твита без медиа файлов"""
    tweets_before = db.session.query(Tweet.id).all()
    tweets_before = {media.id for media in tweets_before}
    resp = client_api_key.post("/api/tweets", json={"tweet_data": "teststestset"})
    assert resp.status_code == 201

    tweets_after = db.session.query(Tweet.id).all()
    tweets_after = {media.id for media in tweets_after}
    assert len(tweets_after) == len(tweets_before) + 1
    tweet = tweets_after - tweets_before
    assert resp.json == {"result": True, "tweet_id": list(tweet)[0]}


def test_create_tweet_with_media(client_api_key, db) -> None:
    """Проверка создания твита с медиафайлами"""
    db.session.add_all(
        [
            Media(id=1, file_path="/", file_name="img1.png"),
            Media(id=2, file_path="/", file_name="img2.png"),
        ]
    )
    db.session.commit()
    resp = client_api_key.post(
        "/api/tweets", json={"tweet_data": "teststestset", "tweet_media_ids": [1, 2]}
    )
    assert resp.status_code == 201
    assert resp.json["result"] is True
    medias = db.session.query(Media.tweet_id).filter(Media.id.in_([1, 2])).all()
    for tweet_id in medias:
        assert tweet_id[0] == resp.json["tweet_id"]


def test_delete_tweet_without_media(client_api_key, db) -> None:
    """Проверка удаления твита без медиа файлов"""
    tweet, *_ = db.session.query(Tweet.id).first()
    resp = client_api_key.delete(f"/api/tweets/{tweet}")
    assert resp.status_code == 201
    assert resp.json["result"] is True

    tweet = db.session.query(Tweet.id).filter(Tweet.id == tweet).first()
    assert tweet is None


def test_delete_tweet_with_media(client_api_key, db) -> None:
    """Проверка удаления твита с медиа файлами"""
    tweet_id, *_ = db.session.query(Tweet.id).first()
    name1, name2 = "img1.png", "img2.png"
    db.session.add_all(
        [
            Media(id=1, file_path=media_dir, file_name=name1, tweet_id=tweet_id),
            Media(id=2, file_path=media_dir, file_name=name2, tweet_id=tweet_id),
        ]
    )
    db.session.commit()
    for name in (name1, name2):
        with open(os.path.join(media_dir, name), "w") as file:
            file.write("nothing")

    resp = client_api_key.delete(f"/api/tweets/{tweet_id}")
    assert resp.status_code == 201
    assert resp.json["result"] is True

    tweet = db.session.query(Tweet.id).filter(Tweet.id == tweet_id).first()
    assert tweet is None
    media = db.session.query(Media.id).filter(Media.tweet_id == tweet_id).all()
    assert media == []
    for name in (name1, name2):
        assert not os.path.exists(os.path.join(media_dir, name))


def test_delete_tweet_fail(client, db) -> None:
    """Проверка удаления чужого твита"""
    tweet_1, tweet_2 = db.session.query(Tweet).filter(Tweet.id.in_([1, 2])).all()
    api_key = tweet_1.user.api_key

    resp = client.delete(f"/api/tweets/{tweet_2.id}", headers={"api_key": api_key})
    assert resp.status_code == 400
    assert resp.json["result"] is False
