import io
import os.path

from app.models import Media
from tests.conftest import media_dir


def test_create_media(client_api_key, db) -> None:
    """Проверка создания файла"""
    image_name = "fake-image-stream.jpg"
    data = {"file": (io.BytesIO(b"some random data"), image_name)}
    response = client_api_key.post("/api/medias", data=data)
    assert response.status_code == 201

    medias = db.session.query(Media).all()
    assert len(medias) == 1
    media_id = medias[0].id
    assert response.json == {"result": True, "media_id": media_id}
    assert os.path.isfile(os.path.join(media_dir, medias[0].file_name))


def test_create_media_fail(client_api_key, db) -> None:
    """Проверка ошибки при создании файла"""
    resp = client_api_key.post("/api/medias", data={})
    assert resp.status_code == 400
    assert resp.json["result"] is False
    assert resp.json.get("error_type")
    assert resp.json.get("error_message")


def test_read_media(client_api_key, db) -> None:
    """Проверка чтения изображения"""
    file_name = "test.png"
    path = os.path.join(media_dir, file_name)
    square = (
        b"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x01\x00\x00\x00\x01\x01\x03\x00\x00\x00%\xdbV\xca\x00"
        b"\x00\x00\x03PLTE\x00\x00\x00\xa7z=\xda\x00\x00\x00\nIDATx\x9cc`\x00\x00\x00\x02\x00\x01H\xaf\xa4q\x00"
        b"\x00\x00\x00IEND\xaeB`\x82"
    )
    with open(path, "wb") as img:
        img.write(square)
    media = Media(file_path=media_dir, file_name=file_name)
    db.session.add(media)
    db.session.commit()
    resp = client_api_key.get(f"/api/medias/{media.id}")
    assert resp.status_code == 200
    assert resp.data == square
